window.addEventListener('load', async ()=>{
    if (navigator.serviceWorker){
        try {
            await navigator.serviceWorker.register('sm.js');
        }catch (e){
            console.log("Service work fail");
        }

    }
});
let id = 1;
let Student = function () {
    this.id = null;
    this.group = "";
    this.firstname = "";
    this.secondName = "";
    this.gender = "";
    this.birthday = "";
    this.status = false;
}

document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("myForm").addEventListener("submit", function(event) {
        event.preventDefault();
        let student = new Student();
        student.id = document.getElementById("id").value;
        student.group = document.getElementById("group").value;
        student.firstname = document.getElementById("firstName").value;
        student.secondName = document.getElementById("secondName").value;
        student.gender = document.getElementById("gender").value;
        student.birthday = document.getElementById("birthday").value;
        student.status = !!document.getElementById("status").checked;
        addOrUpdateStudent(student);
    });
    document.getElementById("submitDelete").addEventListener('click',function (){
        const id = document.getElementById("idOfDelete").value;
        deleteStudent(id);
    });
    document.querySelector('.content').addEventListener('click', function (event) {
        if (event.target.closest("button")?.classList.contains('addOrEdit')){
            openMainModal(event.target.closest("button"));
        }
        if (event.target.closest("button")?.classList.contains('deleteRow')) {
            openWarningModel(event.target.closest("button"));
        }
    });
});
function clearValidation() {
    document.getElementById("gender").classList.remove("is-invalid");
    document.getElementById("firstName").classList.remove("is-invalid");
    document.getElementById("secondName").classList.remove("is-invalid");
    document.getElementById("group").classList.remove("is-invalid");
    document.getElementById("birthday").classList.remove("is-invalid");
}
function addOrUpdateStudent(student) {
    console.log(student);
    $.ajax({
        url: 'add_update_student.php',
        type: 'POST',
        data: student,
        dataType: 'json',
        success: function(data) {
            clearValidation();
            if (!data.status) {
                document.getElementById(data.error.type).classList.add("is-invalid");
            } else {
                if(student.id===""){
                    student.id=id;
                    id++;
                    addStudent(student);
                }else{
                    editStudent(student);
                }
                let modal = bootstrap.Modal.getInstance(document.getElementById("addStudentForm"));
                modal.hide();
            }
        },
        error: function(xhr, status, error) {
            console.error(status + ': ' + error);
        }
    });
}
function deleteStudent(studentId) {
    $.ajax({
        url: 'delete_student.php',
        type: 'POST',
        data: {'id':studentId},
        dataType: 'json',
        success: function(data) {
            if (data.success) {
                const row = getRowByDataAttribute('data-id', studentId);
                row.parentNode.removeChild(row);
                let modal = bootstrap.Modal.getInstance(document.getElementById("deleteWarningModal"));
                modal.hide();
            } else {
                alert("Error deleting item: " + data.error);
            }
        },
        error: function(xhr, status, error) {
            console.error(status + ': ' + error);
        }
    });
}
function transformDateFormat(dateString) {
    let dateObject = new Date(dateString);

    let day = dateObject.getDate();
    let month = dateObject.getMonth() + 1;
    let year = dateObject.getFullYear();

    // Format the date in "DD.MM.YYYY" format
    return `${day < 10 ? '0' + day : day}.${month < 10 ? '0' + month : month}.${year}`;
}
function transformDateFormatToISO(dateString) {
    let parts = dateString.split('.');

    return parts[2] + '-' + parts[1].padStart(2, '0') + '-' + parts[0].padStart(2, '0');
}
function updateModal(student) {
    document.getElementById("id").value = student.id ? student.id : "";
    document.getElementById("group").value = student.group ;
    document.getElementById("firstName").value = student.firstname;
    document.getElementById("secondName").value = student.secondName;
    document.getElementById("gender").value = student.gender;
    document.getElementById("birthday").value = student.birthday ? transformDateFormatToISO(student.birthday) : "";
    document.getElementById("status").checked = student.status;
}
function getRowByDataAttribute(attributeName, attributeValue) {
    const table = document.getElementById('studentsTable');
    const rows = table.getElementsByTagName('tr');
    for (let i = 0; i < rows.length; i++) {
        const row = rows[i];
        if (row.getAttribute(attributeName)==attributeValue) {
            return row;
        }
    }

    return null;
}

function editStudent(student){
    const row = getRowByDataAttribute('data-id', student.id);
    const cols = row.querySelectorAll('td');

    cols[1].setAttribute("data-value", student.group);
    cols[1].textContent = document.querySelector('#group option[value="' + student.group + '"]').textContent;
    cols[2].textContent = student.firstname+" "+student.secondName;
    cols[3].setAttribute("data-value", student.gender);
    cols[3].textContent = document.querySelector('#gender option[value="' + student.gender + '"]').textContent;
    cols[4].textContent = transformDateFormat(student.birthday);
    if(student.status){
        cols[5].innerHTML ='<i class="bi bi-circle-fill status active"></i>'
    }else {
        cols[5].innerHTML ='<i class="bi bi-circle-fill status"></i>'
    }
}
let openMainModal = function (button) {
    clearValidation();
    let student = new Student();
    let title = "Add student";
    if (button.getAttribute("data-id") !== "") {
        title = "Edit student";
        let tr = button.closest('tr');
        let columns = tr.querySelectorAll('td');
        let isActive;
        columns.forEach(column => {
            if(column.querySelector('i.status')){
                isActive =column.querySelector('i.status').classList.contains('active');
            }
        });
        student.id = tr.getAttribute("data-id");
        student.group = columns[1].getAttribute("data-value");
        let name = columns[2].textContent.split(" ");
        student.firstname = name[0];
        student.secondName = name[1];
        student.gender = columns[3].getAttribute("data-value");
        student.birthday = columns[4].textContent;
        student.status = isActive;

    }
    updateModal(student);
    document.getElementById("modalTitle").innerText = title;

    let modal = new bootstrap.Modal(document.getElementById('addStudentForm'));

    modal.show();

}
function openWarningModel(button){
    let tr = button.closest('tr');
    let columns = tr.querySelectorAll('td');
    let name = columns[2].textContent.trim();
    document.getElementById("idOfDelete").value = button.getAttribute("data-id");
    document.getElementById("messageForDelete").innerText = "Are you sure you want to delete student "+name+"?";
    let modal = new bootstrap.Modal(document.getElementById("deleteWarningModal"));

    modal.show();
}
function addStudent(student) {
    let status;
    if(student.status) {
        status = '<i class="bi bi-circle-fill status active"></i>';
    }else {
        status = '<i class="bi bi-circle-fill status"></i>';
    }
    const newRow = document.createElement('tr');
    newRow.setAttribute("data-id",student.id);
    newRow.innerHTML =
        `<td><input type="checkbox" class="table-input"></td>
                    <td data-value = "${student.group}">${document.querySelector('#group option[value="' + student.group + '"]').textContent}</td>
                    <td>${student.firstname+" "+student.secondName}</td>
                    <td data-value="${student.gender}">${document.querySelector('#gender option[value="' + student.gender + '"]').textContent}</td>
                    <td>${transformDateFormat(student.birthday)}</td>
                    <td>
                        ${status}
                    <td>
                        <div class="d-flex justify-content-center">
                            <button  class="btn addOrEdit" data-id="${student.id}">
                                <i class="bi bi-pencil-square edit-btn close-btn table-icons"></i>
                            </button>
                            <button  class="btn deleteRow" data-id="${student.id}">
                                <i class="bi bi-trash3 delete-btn close-btn table-icons"></i>
                            </button>
                        </div>
                    </td>
          `;

    document.getElementById('studentsTable').getElementsByTagName('tbody')[0].appendChild(
        newRow);
}















