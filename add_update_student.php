<?php
header('Content-Type: application/json');

function formError($response,$infoError,$name)
{
    $error = ['message' => $infoError, 'type' =>$name];
    $response['error'] = $error;
    $response['status'] = false;
    echo json_encode($response);
    exit();
}
include "config.php";


if($_SERVER['REQUEST_METHOD'] == "POST") {
    $response = array('status' =>true);
    if (!isset($_POST['group'])||!array_key_exists($_POST['group'], $arrGroup)) {
        formError($response,"Incorrect group",'group');
    }
    if (!isset($_POST['firstname'])) {
        formError($response,"Empty firstName",'firstName');
    }
    if (!isset($_POST['secondName'])) {
        formError($response,"Incorrect secondName enter",'secondName');
    }
    if (!isset($_POST['gender'])||!array_key_exists($_POST['gender'], $arrGender)) {
        formError($response,"Incorrect gender",'gender');
    }
    if (!isset($_POST['birthday'])||!strtotime($_POST['birthday'])) {
        formError($response,"Empty birthday",'birthday');
    }
    if (!isset($_POST['id'])) {
        formError($response,"Empty id",'id');
    }
    echo json_encode($response);
}
