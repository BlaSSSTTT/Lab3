<?php
header('Content-Type: application/json');
if($_SERVER['REQUEST_METHOD'] == "POST"){
    $response = array('success' => true);
    if(isset($_POST['id'])){
        $id = $_POST['id'];
        if (empty($id)) {
            $response['success'] = false;
            $response['error'] = 'No student ID provided';
        }

        echo json_encode($response);
    }else{
        formError($response,"Empty id",'id');
    }
}

